import com.sun.xml.internal.ws.util.StringUtils;

//T04_02_04_A

public class t04_02_04_a {
	
	public static void main(String[] args) {
		
		String cadena = " esto es un ejemplo de una cadena de ejemplo ";
		
		System.out.printf("CADENA: '%s' \n",cadena);
		System.out.printf("Número de caracteres: %d \n",cadena.length());
		
		cadena = cadena.trim();
		
		System.out.printf("Número de caracteres sin los espacios en blanco"
				+ " al principio y al final: %d \n",cadena.length());
		
		System.out.printf("Número de palabras: '%s' \n",cadena.split(" ").length);
		
		cadena = StringUtils.capitalize(cadena);
		
		System.out.printf("La frase emprezando con mayúsculas: '%s' \n",cadena);
		
		String cola = cadena.substring(26, cadena.length());
		System.out.printf("COLA: '%s' \n",cola);
		
		String principio = cadena.substring(0, 25);
		System.out.printf("PRINCIPIO: '%s' \n", principio);
		
		String frase = principio + " " + cola;
		
		System.out.printf("FRASE: '%s' \n",frase);
		
	}
	
}
