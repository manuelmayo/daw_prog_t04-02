//T04_02_02
public class t04_02_02 {
	
	static String decToUni(int num) {
		return Character.toString((char)num);
	}
	
	static int uniToDec(char uni) {
		return (int) uni;
	}
	
	public static void main(String[] args) {
		
		if (args.length > 0) {
			
			try {
				int entrada = Integer.parseInt(args[0]);
				
				if (entrada >= 0 && entrada <= 65535) {
					String unicode = decToUni(entrada);
					System.out.printf("Entrada: <%s> | Unicode: <%s>\n",entrada, unicode);
				}
				else {
					System.out.println("El argumento debe estar en el rango: [0, 65535]");
				}
			} 
			catch(Exception e) {
				char entrada = args[0].charAt(0);
				int decimal = uniToDec(entrada);
				System.out.printf("Entrada: <%s> | Decimal: <%s>\n",entrada, decimal);
			}
		}
		else {
			System.out.println("Debes introducir un argumento");
		}
		
	}
	
}
