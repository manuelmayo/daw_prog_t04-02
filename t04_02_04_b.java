//T04_02_04_B

import java.util.Scanner;

public class t04_02_04_b {
	
	private static final Scanner SCANNER = new Scanner (System.in);

	public static void main(String[] args) {
		
		System.out.print("Introduce la cantidad de euros: ");
		double capital = Double.parseDouble(SCANNER.nextLine());
		
		System.out.print("Introduce la tasa de interés: ");
		double tasa = Double.parseDouble(SCANNER.nextLine());
		
		System.out.print("Introduce el número de años: ");
		int anos = Integer.parseInt(SCANNER.nextLine());

		double capitalFinal = (capital * Math.pow((1+tasa/100), anos));
		double rendimiento = capitalFinal-capital;
		
		Number[] datos = {capital,tasa,anos,capitalFinal,rendimiento};
		
		int maxLength = 0;
		for (int i=0;i<datos.length;i++) {
			int tamDato;
			if (datos[i] instanceof Integer) {
				tamDato = String.format("%d",datos[i]).length();
			} else {
				tamDato = String.format("%.2f",datos[i]).length();
			}
			System.out.println(String.valueOf(datos[i]) + " - "+datos[i]);
			if (maxLength < tamDato) {
				maxLength = tamDato;
			}
		}
		
		int tamMarco = 35 + maxLength;
		String marco = new String(new char[tamMarco]).replace("\0", "#");
		
		System.out.println("\n");
		System.out.println(marco);
		System.out.printf("#%"+(tamMarco-1)+"s \n","#");
		System.out.println(salida(tamMarco,capital,"Capital Inicial:", "€"));
		System.out.println(salida(tamMarco,tasa,"Interes anual:", "%"));
		System.out.println(salida(tamMarco,anos,"Periodo:", "años"));
		System.out.printf("#%"+(tamMarco-1)+"s \n","#");
		System.out.println(salida(tamMarco,capitalFinal,"Capital Final:", "€"));
		System.out.println(salida(tamMarco,rendimiento,"Rendimiento:", "€"));
		System.out.printf("#%"+(tamMarco-1)+"s \n","#");
		System.out.println(marco);
		
	}
	
	public static String salida(int ancho, Number valor, String linea, String simb) {
		int separacion = ancho-(linea.length()+3);
		int separacionSimb = 5-simb.length();
		String cadenaValor;
		if (valor instanceof Integer) {
			cadenaValor = String.format("%d %-"+separacionSimb+"s #",valor, simb);
		} else {
			cadenaValor = String.format("%.2f %-"+separacionSimb+"s #",valor, simb);
		}
		String cadena = String.format(
				"# %s %"+separacion+"s", linea, cadenaValor);
		return cadena;
	}
}
