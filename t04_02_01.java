//T04_02_01
public class t04_02_01 {
	
	public static void main(String[] args) {
		System.out.println("DEC | HEX | CHAR");
		for (int i=0;i<=127;i++) {
			String hexadecimal = Integer.toHexString(i);
			int n_zeros_hex = 3-hexadecimal.length();
			hexadecimal = new String(new char[n_zeros_hex]).replace("\0","0")+hexadecimal;
			if (i>32 && i <= 126) {
				char caracter = (char) i;
				System.out.printf("%03d | %3s | %s\n",i,hexadecimal,caracter);
			} else {
				System.out.printf("%03d | %3s |\n",i,hexadecimal);
			}
			
		}
	}
	
}
