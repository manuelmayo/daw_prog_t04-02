//T04_02_03

import java.util.Scanner;

public class t04_02_03 {
	
	private static Scanner entradaEscaner;
	
	public static void main(String[] args) {
		
		System.out.print("Introduce un número de 5 dígitos: ");
		entradaEscaner = new Scanner (System.in);
		
		try {
			String entrada = entradaEscaner.nextLine();
			if (entrada.length() == 5) {
				for (int i=0;i<entrada.length();i++) {
					System.out.print(entrada.charAt(i)+" ");
				}
			} 
			else {
				System.out.print("El número debe de ser de 5 dígitos");
			}
		}
		catch(Exception e) {
			System.out.print("Debes introducir un número");
		}
		
	}
	
}
